# Instruções básicas para utilizar o projeto Jogo Da Vida de Conway

  *Compilando o projeto*
-Abra o terminal
-Locatize A pasta desse projeto (cd <diretório_do_projeto> )
-Execute o comando 'make', espere um pouco
-Em seguida execute o comando'make run', e o projeto irá rodar

**Obs: Aumente ou maximize o terminal para ficar maior que o campo**

  *Utilizando o projeto*
-Se o projeto já estiver compilado, basta executar 'make run'
-Um menu de opçoẽs deve aparecer
-Há as opções de mostras uma das formas escolhidas, configurar
  n° de ciclos, tempo de execução, etc. opção de customizar campo
  e a de sair
-Basta digitar a tecla correspondente e pressionar enter
